﻿## iquartz

> iquartz 网络任务处理系统，可以通过设置corn任务就行任务管理


### 技术选型：

* **服务端**

* SSH (Spring、SpringMVC、Hibernate）
* 安全权限 Shiro
* 缓存 Ehcache
* 视图模板 freemarker 
* quartz*

###  系统截图

![输入图片说明](http://git.oschina.net/uploads/images/2016/0824/143630_57c7a99f_1029.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0824/143646_d2f85845_1029.jpeg "在这里输入图片标题")

### 开源协议

如果您的网站使用了 iquartz, 请在网站页面页脚处保留 iquartz相关版权信息链接

