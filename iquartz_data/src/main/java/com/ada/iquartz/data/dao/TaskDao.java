package com.ada.iquartz.data.dao;


import  com.ada.data.core.CriteriaDao;
import  com.ada.data.core.Updater;
import com.ada.data.core.Pagination;
import  com.ada.iquartz.data.entity.Task;

public interface TaskDao extends CriteriaDao<Task,Long>{
	public Pagination getPage(int pageNo, int pageSize);

	public Task findById(Long id);

	public Task save(Task bean);

	public Task updateByUpdater(Updater<Task> updater);

	public Task deleteById(Long id);
}