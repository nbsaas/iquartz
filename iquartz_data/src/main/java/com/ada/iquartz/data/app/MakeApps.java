package com.ada.iquartz.data.app;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.ada.area.entity.Area;
import com.ada.imake.ChainMake;
import com.ada.imake.template.hibernate.TemplateHibernateDir;
import com.ada.imake.templates.ace.TemplateAceDir;
import com.ada.iquartz.data.entity.Task;
import com.ada.site.entity.App;
import com.ada.user.entity.UserFeedBack;
import com.ada.user.entity.UserInfo;
import com.ada.user.entity.UserNotification;
import com.ada.user.entity.UserNotificationCatalog;

public class MakeApps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String fieldir = TemplateDir.class.getResource(".").toString();
		//System.out.println(fieldir);

		ChainMake make=	new ChainMake(TemplateAceDir.class,TemplateHibernateDir.class);
		make.setAction("com.ada.iquartz.controller.admin");
		//E:\workspace\iquartz\iquartz_web
		File view=new File("E:\\workspace\\iquartz\\iquartz_web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
		make.setView(view);
		
		List<Class<?>> cs=new ArrayList<Class<?>>();
		cs.add(Task.class);

		make.setDao(true);
		make.setService(true);
		make.setView(true);
		make.setAction(true);
		make.makes(cs);
		System.out.println("ok");

	}

	/**
	 * @param make
	 */
	private static void x(ChainMake make) {
		List<Class<?>> others=new ArrayList<Class<?>>();
		others.add(Area.class);
		others.add(UserNotification.class);
		others.add(UserNotificationCatalog.class);
		others.add(UserFeedBack.class);
		others.add(App.class);

		make.setDao(false);
		make.setService(false);
		//make.makes(others);
	}

}
