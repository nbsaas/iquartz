package com.ada.iquartz;

import java.util.Set;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		try {
			// Grab the Scheduler instance from the Factory
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			
			GroupMatcher<TriggerKey> matcher=GroupMatcher.triggerGroupContains("group");
			Set<TriggerKey> keys=	scheduler.getTriggerKeys(matcher);
			for (TriggerKey triggerKey : keys) {
				//scheduler.getTrigger(triggerKey).get
			}
			// and start it off
			//scheduler.start();

		} catch (SchedulerException se) {
			se.printStackTrace();
		}
	}

	/**
	 * @param scheduler
	 * @throws SchedulerException
	 */
	private static void g(Scheduler scheduler) throws SchedulerException {
		// define the job and tie it to our HelloJob class
		JobDetail job = JobBuilder.newJob(HelloJob.class).withIdentity("job1", "group1").build();
		// Trigger the job to run now, and then repeat every 40 seconds
		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1").startNow()
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(2).repeatForever())
				.build();

		// Tell quartz to schedule the job using our trigger
		scheduler.scheduleJob(job, trigger);
		// scheduler.shutdown();
	}
}
