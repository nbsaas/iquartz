package com.ada.iquartz.task;

import java.io.IOException;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ada.iquartz.data.entity.Task;
import com.ada.iquartz.data.service.TaskService;
import com.ada.iquartz.factory.ObjectFacotry;
import com.young.http.HttpConnection;

public class HttpJob implements Job {

	private Logger logger = LoggerFactory.getLogger("ada");

	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		JobDataMap map = context.getJobDetail().getJobDataMap();
		String url = map.getString("url");
		logger.info("url:" + url);
		logger.info("time:" + new Date().toLocaleString());
		if (url != null) {
			try {
				HttpConnection.connect(url).timeout(20000).execute().body();
				logger.info("执行完成");
			} catch (IOException e) {
				// e.printStackTrace();
				logger.info("执行失败");
			}
		} else {
			logger.info("url为空");
		}

		Long id = map.getLong("id");
		if (id != null) {
			TaskService service = ObjectFacotry.get().getBean(TaskService.class);
			Task task = service.findById(id);
			if (task != null) {
				task.setLastDate(new Date());
				task.setState(1);
				Long num = task.getNums();
				if (num==null) {
					num=0l;
				}
				num=num+1;
				task.setNums(num);
				service.update(task);
			}
		}
	}

}
