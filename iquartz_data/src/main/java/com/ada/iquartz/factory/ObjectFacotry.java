package com.ada.iquartz.factory;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ObjectFacotry {

	
	private static BeanFactory factory;
	public static BeanFactory get(){
		if (factory==null) {
			factory=new ClassPathXmlApplicationContext("applicationContext.xml");
		}
		return factory;
	}
}
