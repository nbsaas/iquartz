package com.ada.iquartz.controller.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "admin")
public class AdminWebAction {

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "admin/login";
	}

	@RequestMapping(value = "loginwork", method = RequestMethod.POST)
	public String loginwork(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "admin/home";
	}

	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response, Model model) {
		init(model);
		return "admin/home";
	}

	/**
	 * @param model
	 */
	private void init(Model model) {
		Scheduler scheduler = getScheduler();
		if (scheduler != null) {
			try {
				model.addAttribute("schedulerName", scheduler.getSchedulerName());
				if (scheduler.isStarted()) {
					model.addAttribute("started", true);
				} else {
					model.addAttribute("started", false);
				}
				if (scheduler.isShutdown()) {
					model.addAttribute("shutdown", true);
				} else {
					model.addAttribute("shutdown", false);
				}
				model.addAttribute("instanceId", scheduler.getSchedulerInstanceId());
				model.addAttribute("nums", scheduler.getMetaData().getNumberOfJobsExecuted());
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	private Scheduler getScheduler() {
		Scheduler scheduler = null;
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return scheduler;
	}

	@RequestMapping(value = "home")
	public String home(HttpServletRequest request, HttpServletResponse response, Model model) {
		init(model);
		return "admin/home";
	}

	@RequestMapping(value = "run")
	public String run(HttpServletRequest request, HttpServletResponse response, Model model) {
		Scheduler scheduler = getScheduler();
		if (scheduler != null) {
			try {
				if (scheduler.isStarted()) {
				} else {
					scheduler.start();
				}
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		init(model);
		return "admin/home";
	}

	@RequestMapping(value = "stop")
	public String stop(HttpServletRequest request, HttpServletResponse response, Model model) {
		Scheduler scheduler = getScheduler();
		if (scheduler != null) {
			try {
				if (scheduler.isStarted()) {
					scheduler.shutdown();
				} else {

				}
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		init(model);
		return "admin/home";
	}

	@RequestMapping(value = "resumeall")
	public String resumeAll(HttpServletRequest request, HttpServletResponse response, Model model) {
		Scheduler scheduler = getScheduler();
		if (scheduler != null) {
			try {
				scheduler.resumeAll();
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		init(model);
		return "admin/home";
	}

	@RequestMapping(value = "pauseall")
	public String pauseAll(HttpServletRequest request, HttpServletResponse response, Model model) {
		Scheduler scheduler = getScheduler();
		if (scheduler != null) {
			try {
				scheduler.pauseAll();
			} catch (SchedulerException e) {
				e.printStackTrace();
			}
		}
		init(model);
		return "admin/home";
	}
}
