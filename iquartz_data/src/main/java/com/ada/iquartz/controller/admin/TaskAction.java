package com.ada.iquartz.controller.admin;

import javax.servlet.http.HttpServletRequest;

import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ada.data.page.Order;
import com.ada.data.page.Page;
import com.ada.data.page.Pageable;
import com.ada.iquartz.data.entity.Task;
import com.ada.iquartz.data.service.TaskService;
import com.ada.iquartz.task.HttpJob;

@Controller
public class TaskAction {
	private static final Logger log = LoggerFactory.getLogger(TaskAction.class);

	/**
	 * 
	 */
	private Scheduler getScheduler() {
		Scheduler scheduler = null;
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return scheduler;
	}

	@RequestMapping("/admin/task/view_list")
	public String list(Pageable pageable, HttpServletRequest request, ModelMap model) {

		if (pageable == null) {
			pageable = new Pageable();
		}
		if (pageable.getOrders() == null || pageable.getOrders().size() == 0) {
			pageable.getOrders().add(Order.desc("id"));
		}
		if (pageable.getSearchProperty() == null || pageable.getSearchProperty().length() < 2) {
			pageable.setSearchProperty("name");
			pageable.setSearchValue("");
		}
		Page<Task> pagination = manager.findPage(pageable);
		model.addAttribute("list", pagination.getContent());
		model.addAttribute("page", pagination);
		return "/admin/task/list";
	}

	@RequestMapping("/admin/task/view_add")
	public String add(ModelMap model) {
		return "/admin/task/add";
	}

	@RequestMapping("/admin/task/view_edit")
	public String edit(Pageable pageable, Long id, Integer pageNo, HttpServletRequest request, ModelMap model) {
		model.addAttribute("model", manager.findById(id));
		model.addAttribute("pageNo", pageNo);
		model.addAttribute("page", pageable);
		return "/admin/task/edit";
	}

	@RequestMapping("/admin/task/view_view")
	public String view(Long id, HttpServletRequest request, ModelMap model) {
		model.addAttribute("model", manager.findById(id));
		return "/admin/task/view";
	}

	@RequestMapping("/admin/task/model_save")
	public String save(Task bean, HttpServletRequest request, ModelMap model) {

		String view = "redirect:view_list.htm";
		if (bean.getCron() == null || !CronExpression.isValidExpression(bean.getCron())) {
			model.addAttribute("erro", "cron无效");
			return "/admin/task/add";
		}
		try {
			bean = manager.save(bean);

			Scheduler scheduler = getScheduler();
			Trigger trigger = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronSchedule(bean.getCron()))
					.withIdentity("trigger" + bean.getId(), "group" + bean.getId()).build();
			JobDetail jobDetail = JobBuilder.newJob(HttpJob.class)
					.withIdentity("job" + bean.getId(), "group" + bean.getId()).usingJobData("url", bean.getUrl())
					.usingJobData("id", bean.getId()).build();

			scheduler.scheduleJob(jobDetail, trigger);
			log.info("save object id={}", bean.getId());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("erro", e.getMessage());
			view = "/admin/task/add";
		}
		return view;
	}

	@RequestMapping("/admin/task/model_update")
	public String update(Pageable pageable, Task bean, HttpServletRequest request, ModelMap model) {

		String view = "redirect:/admin/task/view_list.htm?pageNumber=" + pageable.getPageNumber();
		if (bean.getCron() == null || !CronExpression.isValidExpression(bean.getCron())) {
			model.addAttribute("erro", "cron无效");
			model.addAttribute("model",bean);
			model.addAttribute("pageNo", pageable.getPageNumber());
			model.addAttribute("page", pageable);
			return "/admin/task/edit";
		}
		try {
			bean = manager.update(bean);
			Scheduler scheduler = getScheduler();
			TriggerKey triggerKey = TriggerKey.triggerKey("trigger" + bean.getId(), "group" + bean.getId());
			scheduler.pauseTrigger(triggerKey);
			scheduler.unscheduleJob(triggerKey);
			
			
			Trigger trigger = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronSchedule(bean.getCron()))
					.withIdentity("trigger" + bean.getId(), "group" + bean.getId()).build();
			JobDetail jobDetail = JobBuilder.newJob(HttpJob.class)
					.withIdentity("job" + bean.getId(), "group" + bean.getId()).usingJobData("url", bean.getUrl())
					.usingJobData("id", bean.getId()).build();

			scheduler.scheduleJob(jobDetail, trigger);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("erro", e.getMessage());
			model.addAttribute("model", bean);
			model.addAttribute("page", pageable);
			view = "/admin/task/edit";
		}
		return view;
	}

	@RequestMapping("/admin/task/model_delete")
	public String delete(Pageable pageable, Long id, HttpServletRequest request, ModelMap model) {

		try {
			manager.deleteById(id);

			Scheduler scheduler = getScheduler();
			TriggerKey triggerKey = TriggerKey.triggerKey("trigger" + id, "group" + id);
			scheduler.pauseTrigger(triggerKey);
			scheduler.unscheduleJob(triggerKey);
		} catch (DataIntegrityViolationException e) {
			model.addAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return list(pageable, request, model);
	}

	@RequestMapping("/admin/task/model_deletes")
	public String deletes(Pageable pageable, Long[] ids, HttpServletRequest request, ModelMap model) {

		try {
			manager.deleteByIds(ids);
		} catch (DataIntegrityViolationException e) {
			model.addAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
		}
		return list(pageable, request, model);
	}

	@RequestMapping("/admin/task/pause")
	public String pause(Pageable pageable, Long id, HttpServletRequest request, ModelMap model) {

		String view = "redirect:/admin/task/view_list.htm?pageNumber=" + pageable.getPageNumber();
		try {
			Scheduler scheduler = getScheduler();
			TriggerKey triggerKey = TriggerKey.triggerKey("trigger" + id, "group" + id);
			scheduler.pauseTrigger(triggerKey);
			Task task = manager.findById(id);
			if (task != null) {
				task.setState(0);
				manager.update(task);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}

	@RequestMapping("/admin/task/resume")
	public String resume(Pageable pageable, Long id, HttpServletRequest request, ModelMap model) {

		String view = "redirect:/admin/task/view_list.htm?pageNumber=" + pageable.getPageNumber();
		try {
			Scheduler scheduler = getScheduler();
			TriggerKey triggerKey = TriggerKey.triggerKey("trigger" + id, "group" + id);
			scheduler.resumeTrigger(triggerKey);
			Task task = manager.findById(id);
			if (task != null) {
				task.setState(0);
				manager.update(task);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}

	@Autowired
	private TaskService manager;
}